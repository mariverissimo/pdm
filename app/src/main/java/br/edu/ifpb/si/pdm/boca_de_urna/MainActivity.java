package br.edu.ifpb.si.pdm.boca_de_urna;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Contador contador;
    private TextView textView;

    public MainActivity(){
        this.contador = new Contador();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.textView = (TextView) findViewById(R.id.tvNumero);
    }

    public void onClickInc(View view){
        this.contador.incrementa();
        //Mensagens de log
        Log.i("bu", "Incrementou - "+ this.contador.getValor());
        this.textView.setText(Integer.toString(this.contador.getValor()));
    }

    public void onClickDec(View view){
        this.contador.decrementa();
        Log.i("bu", "Decrementou - " + this.contador.getValor());
        this.textView.setText(Integer.toString(this.contador.getValor()));
    }

}
